import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import { OSM, Vector as VectorSource, BingMaps } from 'ol/source';
import { Tile as TileLayer, Vector as VectorLayer, Group as LayerGroup } from 'ol/layer';
import { defaults as defaultInteractions, DragRotateAndZoom } from 'ol/interaction';
import { defaults as defaultControls, OverviewMap, FullScreen, Attribution, ScaleLine, ZoomSlider } from 'ol/control';
import { createStringXY } from 'ol/coordinate';
import GeoJSON from 'ol/format/GeoJSON';
import MousePosition from 'ol/control/MousePosition';
import XYZ from 'ol/source/XYZ';
import { toStringHDMS } from 'ol/coordinate';
import { toLonLat } from 'ol/proj';
import TileJSON from 'ol/source/TileJSON';
import Overlay from 'ol/Overlay';
import { unByKey } from 'ol/Observable';
import { getArea, getLength } from 'ol/sphere';
import { LineString, Polygon } from 'ol/geom';
import Draw from 'ol/interaction/Draw';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style';
import { toJpeg } from 'html-to-image';
import Select from 'ol/interaction/Select';
import Feature from 'ol/Feature';
import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';
import { fromLonLat } from 'ol/proj';

const container = document.getElementById('popup');
const content = document.getElementById('popup-content');
const closer = document.getElementById('popup-closer');
const key = 'pk.eyJ1IjoiZXZlbmR5eCIsImEiOiJjazF1ZDQ1aTEwaGxyM2NvMHR0dmFxaGEyIn0.JT7I3g-VsGRWQL6Twp01Rg';

const mousePositionControl = new MousePosition({
  coordinateFormat: createStringXY(4),
  projection: 'EPSG:4326',
  className: 'custom-mouse-position',
  target: document.getElementById('mouse-position'),
  undefinedHTML: '&nbsp;'
});

var mapOverview = new OverviewMap({
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ]
});

var layers = [
  new TileLayer({
    name: 'osmlayers',
    source: new OSM(),
    type: 'base',
    title: 'OpenStreetMap',
    visible: true
  }),

  new VectorLayer({
    name: 'bataskab',
    source: new VectorSource({
      format: new GeoJSON(),
      url: '../resources/assets/geojson/batas-kabupaten.geojson',
      crossOrigin: 'anonymous'
    }),
    visible: false
  }),

  new VectorLayer({
    name: 'bataskec',
    source: new VectorSource({
      format: new GeoJSON({
        defaultDataProjection: 'EPSG:4326'
      }),
      url: '../resources/assets/geojson/batas-kecamatan.geojson',
      crossOrigin: 'anonymous'
    }),
    visible: false
  }),

  new TileLayer({
    name: 'batasnegara',
    source: new TileJSON({
      url: 'https://api.tiles.mapbox.com/v4/mapbox.world-borders-light.json?secure&access_token=' + key,
      crossOrigin: 'anonymous'
    }),
    visible: false
  }),
  new VectorLayer({
    name: 'batasjalan',
    source: new VectorSource({
      format: new GeoJSON({
        defaultDataProjection: 'EPSG:4326'
      }),
      url: '../resources/assets/geojson/jalan.geojson',
      crossOrigin: 'anonymous'
    }),
    visible: false
  }),
  new VectorLayer({
    name: 'point0',
    source: new VectorSource({
      url: 'http://localhost:8000/api/v1/geojson/1',
      crossOrigin: 'anonymous',
      format: new GeoJSON(),
      wrapX: false
    }),
    visible: false
  })
];
var source = new VectorSource();
var measure = new VectorLayer({
  source: source,
  style: new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.3)'
    }),
    stroke: new Stroke({
      color: '#5f53a9',
      width: 2
    })
  })
});
var overlay = new Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  }
});

var map = new Map({
  target: 'map',
  interactions: defaultInteractions().extend([new DragRotateAndZoom()]),
  controls: defaultControls().extend([
    mousePositionControl,
    mapOverview,
    new FullScreen(),
    new ZoomSlider(),
    new ScaleLine()
  ]),
  layers: layers,
  overlays: [overlay],
  view: new View({
    center: fromLonLat([109.5697854, -7.0838132]),
    zoom: 8
  })
});

$('.option').click(function() {
  var lid = $(this).attr('id');
  map.getLayers().forEach(function(layer) {
    if (layer.get('name') == lid) {
      var visible = layer.getVisible();
      if (visible == false) {
        layer.setVisible(true);
      }
      if (visible == true) {
        layer.setVisible(false);
      }
    }
  });
});

map.on('singleclick', showInfo);
function showInfo(evt) {
  var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
    return feature;
  });
  if (feature) {
    var coordinate = evt.coordinate;
    var properties = feature.getProperties(),
      property;
    content.innerHTML = '';
    for (property in properties) {
      content.innerHTML +=
        '<label><b>' +
        property +
        '</b></label> : <br/><input class="value" value="' +
        properties[property] +
        '"></input><br/>';
    }
    overlay.setPosition(coordinate);
    return;
  } else {
    overlay.setPosition(undefined);
  }
}

closer.onclick = function() {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};

//Measure Drawing
var sketch;
var helpTooltipElement;
var helpTooltip;
var measureTooltipElement;
var measureTooltip;
var continuePolygonMsg = 'Click to continue drawing the polygon';
var continueLineMsg = 'Click to continue drawing the line';
var typeSelect = document.getElementById('type');
var draw;
var formatLength = function(line) {
  var length = getLength(line);
  var output;
  if (length > 100) {
    output = Math.round((length / 1000) * 100) / 100 + ' ' + 'km';
  } else {
    output = Math.round(length * 100) / 100 + ' ' + 'm';
  }
  return output;
};
var formatArea = function(polygon) {
  var area = getArea(polygon);
  var output;
  if (area > 10000) {
    output = Math.round((area / 1000000) * 100) / 100 + ' ' + 'km<sup>2</sup>';
  } else {
    output = Math.round(area * 100) / 100 + ' ' + 'm<sup>2</sup>';
  }
  return output;
};

function addInteraction() {
  var type = typeSelect.value == 'area' ? 'Polygon' : 'LineString';
  draw = new Draw({
    source: source,
    type: type,
    style: new Style({
      fill: new Fill({
        color: 'rgba(255, 255, 255, 0.2)'
      }),
      stroke: new Stroke({
        color: 'rgba(0, 0, 0, 0.5)',
        lineDash: [10, 10],
        width: 2
      }),
      image: new CircleStyle({
        radius: 5,
        stroke: new Stroke({
          color: 'rgba(0, 0, 0, 0.7)'
        }),
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)'
        })
      })
    })
  });
  map.addInteraction(draw);

  createMeasureTooltip();
  createHelpTooltip();

  var listener;
  draw.on('drawstart', function(evt) {
    // set sketch
    sketch = evt.feature;
    var tooltipCoord = evt.coordinate;
    listener = sketch.getGeometry().on('change', function(evt) {
      var geom = evt.target;
      var output;
      if (geom instanceof Polygon) {
        output = formatArea(geom);
        tooltipCoord = geom.getInteriorPoint().getCoordinates();
      } else if (geom instanceof LineString) {
        output = formatLength(geom);
        tooltipCoord = geom.getLastCoordinate();
      }
      measureTooltipElement.innerHTML = output;
      measureTooltip.setPosition(tooltipCoord);
    });
  });

  draw.on('drawend', function() {
    measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
    measureTooltip.setOffset([0, -7]);
    // unset sketch
    sketch = null;
    // unset tooltip so that a new one can be created
    measureTooltipElement = null;
    createMeasureTooltip();
    unByKey(listener);
  });
}
function createHelpTooltip() {
  if (helpTooltipElement) {
    helpTooltipElement.parentNode.removeChild(helpTooltipElement);
  }
  helpTooltipElement = document.createElement('div');
  helpTooltipElement.className = 'ol-tooltip hidden';
  helpTooltip = new Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
    positioning: 'center-left'
  });
  map.addOverlay(helpTooltip);
}
function createMeasureTooltip() {
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  measureTooltipElement = document.createElement('div');
  measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
  measureTooltip = new Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  });
  map.addOverlay(measureTooltip);
}
typeSelect.onchange = function() {
  map.removeInteraction(draw);
  addInteraction();
};

document.getElementById('measure').addEventListener('click', drawFunction);
function drawFunction() {
  var pointerMoveHandler = function(evt) {
    if (evt.dragging) {
      return;
    }
    var helpMsg = 'Start drawing';
    if (sketch) {
      var geom = sketch.getGeometry();
      if (geom instanceof Polygon) {
        helpMsg = continuePolygonMsg;
      } else if (geom instanceof LineString) {
        helpMsg = continueLineMsg;
      }
    }
    helpTooltipElement.innerHTML = helpMsg;
    helpTooltip.setPosition(evt.coordinate);
    helpTooltipElement.classList.remove('hidden');
  };
  map.on('pointermove', pointerMoveHandler);
  map.getViewport().addEventListener('mouseout', function() {
    helpTooltipElement.classList.add('hidden');
  });
  addInteraction();
}

document.getElementById('remove').addEventListener('click', removeFunction);
function removeFunction() {
  map.removeInteraction(draw);
}
//EXPORT
var dims = {
  a0: [1189, 841],
  a1: [841, 594],
  a2: [594, 420],
  a3: [420, 297],
  a4: [297, 210],
  a5: [210, 148]
};
var exportOptions = {
  filter: function(element) {
    return element.className.indexOf('ol-control') === -1;
  }
};
var exportButton = document.getElementById('export-pdf');

exportButton.addEventListener(
  'click',
  function() {
    exportButton.disabled = true;
    document.body.style.cursor = 'progress';
    var format = document.getElementById('format').value;
    var resolution = document.getElementById('resolution').value;
    var dim = dims[format];
    var width = Math.round((dim[0] * resolution) / 25.4);
    var height = Math.round((dim[1] * resolution) / 25.4);
    var size = map.getSize();
    var viewResolution = map.getView().getResolution();

    map.once('rendercomplete', function() {
      exportOptions.width = width;
      exportOptions.height = height;
      toJpeg(map.getViewport(), exportOptions).then(function(dataUrl) {
        var pdf = new jsPDF('landscape', undefined, format);
        pdf.addImage(dataUrl, 'JPEG', 0, 0, dim[0], dim[1]);
        pdf.save('map.pdf');
        // Reset original map size
        map.setSize(size);
        map.getView().setResolution(viewResolution);
        exportButton.disabled = false;
        document.body.style.cursor = 'auto';
      });
    });
    // Set print size
    var printSize = [width, height];
    map.setSize(printSize);
    var scaling = Math.min(width / size[0], height / size[1]);
    map.getView().setResolution(viewResolution / scaling);
  },
  false
);
