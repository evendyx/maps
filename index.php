<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Laravel Maps</title>
  <link rel="icon" href="images/Fevicon.png" type="image/png">

  <!-- Style -->
  <link rel="stylesheet" href="../public/css/maps.css">
  <link rel="stylesheet" href="../public/vendors/fontawesome/css/all.min.css">
</head>

<body>
  <div class="header">
    <div class="menu">
      <header class="header_area">
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container">

          </div>
        </nav>
      </header>

      <div class="topnav">
        <a id="show-sidebar" class=" active" href="#">
          <i class="fas fa-bars"></i>
        </a>
        <li class="nav-menu showed">
          <a class="tooltip"><button class="btn"><i class="fas fa-map-marker-alt"></i></button>
            <span class="tooltiptext">My Locations</span>
          </a>
        </li>
        <li class="nav-menu showed">
          <a id="measure" class="option-menu measure">
            <form class="form-inline">
              <select id="type">
                <option value="null">Measure</option>
                <option value="length">Length (LineString)</option>
                <option value="area">Area (Polygon)</option>
              </select>
            </form>
          </a>
        </li>
        <li class="nav-menu showed">
          <a class="tooltip"><button class="btn" id="remove"><i class="fas fa-drafting-compass"></i></button>
            <span class="tooltiptext">Remove</span>
          </a>
        </li>
        <li class="nav-menu showed">
          <a class="tooltip"><button id="export" class="btn btn-export"><i class="fas fa-file-export"></i></button>
            <span class="tooltiptext">Export</span>
          </a>
        </li>
        <li class="nav-menu showed">
          <a class="tooltip"><button class="btn"><i class="fas fa-print"></i></button>
            <span class="tooltiptext">Print</span>
          </a>
        </li>
        <li class="nav-menu showed">
          <a class="tooltip"><button class="btn"><i class="fas fa-share-alt"></i></button>
            <span class="tooltiptext">Share</span>
          </a>
        </li>
        <li class="drop">
          <div class="user-avatar mr-2">
            <img src="../public/images/user.jpg">
          </div>
          <div class="dropdownContain">
            <div class="dropOut">
              <ul>
                <li><span aria-hidden="true" class="icon-envelope-alt"></span> Messaggi</li>
                <li><span aria-hidden="true" class="icon-user"></span> Mio Profilo</li>
                <li><span aria-hidden="true" class="icon-cog"></span> Impostazioni</li>
                <li><span aria-hidden="true" class="icon-off"></span> Log Out</li>
              </ul>
            </div>
          </div>
        </li>
      </div>
    </div>
  </div>
  <div class="page-wrapper chiller-theme toggled">
    <nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content shown">
        <div class="sidebar-brand">
          <a href="{{ url('/', []) }}">
            <h2>PT. Parason</h2>
          </a>
          <div id="close-sidebar">
            <i class="fas fa-times"></i>
          </div>
        </div>
        <!-- sidebar-header  -->
        <div class="sidebar-search">
          <div class="search-container">
            <form action="#">
              <input type="text" placeholder="Search.." name="search">
              <button type="submit"><i class="fa fa-search"></i></button>
            </form>
          </div>
        </div>
        <!-- sidebar-search  -->
        <div id="layers" class="ol-unselectable ol-control layer-switcher"></div>
        <div class="sidebar-menu panel">
          <ul>
            <li class="header-menu">
              <span>General</span>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="fas fa-map-marked"></i>
                <span>Basemaps</span>
              </a>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <label class="option-menu">OpenStreet Maps
                      <input type="radio" class="option" name="radio" checked id="osmlayers">
                      <span class="checkmark"></span>
                    </label>
                  </li>
                  <li>
                    <label class="option-menu">Google Maps
                      <input type="radio" class="option" name="radio" id="googlelayers">
                      <span class="checkmark"></span>
                    </label>
                  </li>
                  <li>
                    <label class="option-menu">Bing Maps
                      <input type="radio" class="option" name="radio">
                      <span class="checkmark"></span>
                    </label>
                  </li>
                </ul>
              </div>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="fas fa-layer-group"></i>
                <span>Layer Batas</span>
              </a>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <fieldset id="layer6">
                      <label class="option-menu">Jalan Provinsi
                        <input type="checkbox" class="option" name="checkbox" id="batasnegara">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer7">
                      <label class="option-menu">Jalan Kabupaten
                        <input type="checkbox" class="option" name="checkbox" id="bataskab">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer8">
                      <label class="option-menu">Jalan Kecamatan
                        <input type="checkbox" class="option" name="checkbox" id="bataskec">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer9">
                      <label class="option-menu">Jalan Desa
                        <input type="checkbox" class="option" name="checkbox" id="batasjalan">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                </ul>
              </div>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="fas fa-layer-group"></i>
                <span>Layer Wilayah</span>
              </a>
              <div id="layer1" class="sidebar-submenu">
                <ul>
                  <li>
                    <fieldset id="layer10">
                      <label class="option-menu">Wilayah Provinsi
                        <input type="checkbox" class="option" name="checkbox">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer11">
                      <label class="option-menu">Wilayah Kabupaten
                        <input type="checkbox" class="option" name="checkbox">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer12">
                      <label class="option-menu">Wilayah Kecamatan
                        <input type="checkbox" class="option" name="checkbox">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer13">
                      <label class="option-menu">Wilayah Desa
                        <input type="checkbox" class="option" name="checkbox">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer14">
                      <label class="option-menu">Point 00
                        <input type="checkbox" class="option" name="checkbox" id="point0">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                </ul>
              </div>
            </li>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="far fa-gem"></i>
                <span>Components</span>
              </a>
              <div class="sidebar-submenu">
                <ul>
                <li>
                    <fieldset id="layer15">
                      <label class="option-menu">Layer 15
                        <input type="checkbox" class="option" name="checkbox" id="layer15">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer16">
                      <label class="option-menu">Layer 16
                        <input type="checkbox" class="option" name="checkbox" id="layer16">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer17">
                      <label class="option-menu">Layer 17
                        <input type="checkbox" class="option" name="checkbox" id="layer17">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer18">
                      <label class="option-menu">Layer 18
                        <input type="checkbox" class="option" name="checkbox" id="layer18">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer19">
                      <label class="option-menu">Layer 19
                        <input type="checkbox" class="option" name="checkbox" id="layer19">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                  <li>
                    <fieldset id="layer20">
                      <label class="option-menu">Layer 20
                        <input type="checkbox" class="option" name="checkbox" id="layer20">
                        <span class="checkmark"></span>
                      </label>
                    </fieldset>
                  </li>
                </ul>
              </div>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="fa fa-chart-line"></i>
                <span>Charts</span>
              </a>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="#">Pie chart</a>
                  </li>
                  <li>
                    <a href="#">Line chart</a>
                  </li>
                  <li>
                    <a href="#">Bar chart</a>
                  </li>
                  <li>
                    <a href="#">Histogram</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="sidebar-dropdown">
              <a href="#">
                <i class="fas fa-shapes"></i>
                <span>Shapes</span>
              </a>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="#">Line</a>
                  </li>
                  <li>
                    <a href="#">Oval</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="header-menu">
              <span>Extra</span>
            </li>
            <li>
              <a href="{{ url('/api/documentation') }}">
                <i class="fa fa-book"></i>
                <span>Documentation</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-folder"></i>
                <span>Examples</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="page-content">
      <div id="map" class="map"></div>
      <div id="mouse-position"></div>
    </div>
    <div id="popup" class="ol-popup">
      <div class="sidebar-brand">
        <h2>Detail Information</h2>
        <div id="close-sidebar">
          <a href="#" id="popup-closer" class="ol-popup-closer"></a>
        </div>
      </div>
      <div id="popup-content"></div>
    </div>
    <!-- The Modal Export-->
    <div id="export-dialog" class="export-modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Export</h2>
        <form class="form">
          <div>
            <label>Page size </label>
            <select id="format">
              <option value="a0">A0 (slow)</option>
              <option value="a1">A1</option>
              <option value="a2">A2</option>
              <option value="a3">A3</option>
              <option value="a4" selected>A4</option>
              <option value="a5">A5 (fast)</option>
            </select>
          </div>
          <div> <label>Resolution </label>
            <select id="resolution">
              <option value="72">72 dpi (fast)</option>
              <option value="150">150 dpi</option>
              <option value="300">300 dpi (slow)</option>
            </select>
          </div>
        </form>
        <button id="export-pdf" class="btn btn-save">Export PDF</button>
      </div>
    </div>
  </div>

  <!-- Script -->
  <script type="text/javascript" src="../public/vendors/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="../public/js/maps.js"></script>
  <script type="text/javascript">
    jQuery(function($) {
      $(".sidebar-dropdown > a").click(function() {
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
          .parent()
          .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
      });


      $("#show-sidebar").click(function() {
        $(".page-wrapper").addClass("toggled");
        $(".nav-menu").addClass("showed");
      });
      $("#close-sidebar").click(function() {
        $(".page-wrapper").removeClass("toggled");
        $(".nav-menu").removeClass("showed");
      });

      $(window).on('resize', function() {
        var win = $(this);
        if (win.width() > 720) {
          $('.page-wrapper').addClass('toggled');
          $(".nav-menu").addClass("showed");
        } else {
          $('.page-wrapper').removeClass('toggled');
          $(".nav-menu").removeClass("showed");
        }
      });
    });

    var modal = document.getElementById("export-dialog");
    var btn = document.getElementById("export");
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
      modal.style.display = "block";
    }
    span.onclick = function() {
      modal.style.display = "none";
    }
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  </script>
</body>

</html>