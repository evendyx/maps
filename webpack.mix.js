const mix = require('laravel-mix');

mix.js('resources/js/maps.js', 'public/js').sass('resources/sass/maps.scss', 'public/css');
